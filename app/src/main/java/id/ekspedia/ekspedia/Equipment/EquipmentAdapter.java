package id.ekspedia.ekspedia.Equipment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.ekspedia.ekspedia.ItemClickListener;
import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class EquipmentAdapter extends RecyclerView.Adapter<EquipmentAdapter.RecyclerViewHolderEquipment> {

    Context context;
    LayoutInflater inflater;
    ArrayList<Equipment> equipment;
    ArrayList<Equipment> equipments;


    public EquipmentAdapter(Context context, ArrayList<Equipment> equipment) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.equipment = equipment;
    }

    public EquipmentAdapter(Context context, LayoutInflater inflater, ArrayList<Equipment> equipments) {
        this.context = context;
        this.inflater = inflater;
        this.equipments = equipments;
    }

    @Override
    public EquipmentAdapter.RecyclerViewHolderEquipment onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list_item_equipment, parent, false);
        RecyclerViewHolderEquipment viewHolderEquipment = new RecyclerViewHolderEquipment(v);
        return viewHolderEquipment;
    }

    @Override
    public void onBindViewHolder(EquipmentAdapter.RecyclerViewHolderEquipment holder, int position) {
        final int gambar = equipment.get(position).getImageEquipment();
        final String deskripsi = equipment.get(position).getDeskripsiEquipment();

        holder.tvEquipment.setText(deskripsi);
        holder.ivEquipment.setImageResource(gambar);
        holder.tvEquipment.setTag(holder);
        holder.ivEquipment.setTag(holder);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                openDetailActivity(deskripsi, gambar);
            }
        });

    }

    @Override
    public int getItemCount() {
        return equipment.size();
    }

    private void openDetailActivity(String deskripsi, int image) {
        Intent i = new Intent(context, DetailEquipmentActivity.class);
        i.putExtra("deskripsi", deskripsi);
        i.putExtra("gambar", image);
        context.startActivity(i);
    }

    public class RecyclerViewHolderEquipment extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvEquipment;
        ImageView ivEquipment;
        ItemClickListener itemClickListener;

        public RecyclerViewHolderEquipment(View itemView) {
            super(itemView);

            tvEquipment = (TextView)itemView.findViewById(R.id.title_equipment);
            ivEquipment = (ImageView)itemView.findViewById(R.id.image_equipment);

            itemView.setOnClickListener(this);

        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }
}
