package id.ekspedia.ekspedia.Equipment;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class DetailEquipment {

    private String name;
    private int thumbnail;
    private int price;

    public DetailEquipment() {
    }

    public DetailEquipment(String name, int price, int thumbnail) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
