package id.ekspedia.ekspedia.Equipment;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class Equipment {
    String deskripsiEquipment;
    int imageEquipment;

    public Equipment() {
    }

    public String getDeskripsiEquipment() {
        return deskripsiEquipment;
    }

    public void setDeskripsiEquipment(String deskripsiEquipment) {
        this.deskripsiEquipment = deskripsiEquipment;
    }

    public int getImageEquipment() {
        return imageEquipment;
    }

    public void setImageEquipment(int imageEquipment) {
        this.imageEquipment = imageEquipment;
    }
}
