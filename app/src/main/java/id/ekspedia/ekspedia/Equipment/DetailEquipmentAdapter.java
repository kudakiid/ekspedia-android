package id.ekspedia.ekspedia.Equipment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.ekspedia.ekspedia.ItemClickListener;
import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class DetailEquipmentAdapter extends RecyclerView.Adapter<DetailEquipmentAdapter.MyViewHolder>{

    private Context context;
    private List<DetailEquipment> detailEquipments;


    public DetailEquipmentAdapter(Context context, List<DetailEquipment> detailEquipments) {
        this.context = context;
        this.detailEquipments = detailEquipments;
    }

    @Override
    public DetailEquipmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_equipment, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DetailEquipmentAdapter.MyViewHolder holder, int position) {
        final DetailEquipment detailEquipment = detailEquipments.get(position);
        final int gambar = detailEquipments.get(position).getThumbnail();
        final int harga = detailEquipments.get(position).getPrice();

        holder.name.setText(detailEquipment.getName());
        holder.price.setText(String.valueOf(detailEquipment.getPrice()));
        holder.thumbnail.setImageResource(detailEquipment.getThumbnail());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                openDetailActivity(harga,gambar);
            }
        });
    }

    @Override
    public int getItemCount() {
        return detailEquipments.size();
    }

    private void openDetailActivity(int price, int image) {
        Intent i = new Intent(context, DetailEquipmentActivity2.class);
        i.putExtra("harga", price);
        i.putExtra("gambar", image);
        context.startActivity(i);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView name, price;
        public ImageView thumbnail;
        ItemClickListener itemClickListener;

        public MyViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);

            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }
}
