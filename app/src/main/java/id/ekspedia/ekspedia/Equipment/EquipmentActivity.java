package id.ekspedia.ekspedia.Equipment;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import id.ekspedia.ekspedia.BottomNavigationViewHelper;
import id.ekspedia.ekspedia.Explore.ExploreActivity;
import id.ekspedia.ekspedia.MainActivity;
import id.ekspedia.ekspedia.Porter.PorterActivity;
import id.ekspedia.ekspedia.Preparation.PreparationActivity;
import id.ekspedia.ekspedia.R;

public class EquipmentActivity extends AppCompatActivity {

    RecyclerView recyclerView, recyclerView2, recyclerView3;
    RecyclerView.LayoutManager layoutManager, layoutManager2, layoutManager3;

    public EquipmentActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView2 = (RecyclerView)findViewById(R.id.recycler_view_2);
        recyclerView3 = (RecyclerView)findViewById(R.id.recycler_view_3);

        EquipmentAdapter adapter = new EquipmentAdapter(this, KoleksiOutdoor.getEquipment());
        EquipmentAdapter adapter1 = new EquipmentAdapter(this, KoleksiBackpack.getEquipment2());
        EquipmentAdapter adapter2 = new EquipmentAdapter(this, KoleksiFootwear.getEquipment3());

        layoutManager = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);
        layoutManager2 = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);
        layoutManager3 = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);

        recyclerView.setAdapter(adapter);
        recyclerView2.setAdapter(adapter1);
        recyclerView3.setAdapter(adapter2);
        recyclerView.setHasFixedSize(true);
        recyclerView2.setHasFixedSize(true);
        recyclerView3.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView3.setLayoutManager(layoutManager3);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_explore:
                        Intent intent1 = new Intent(EquipmentActivity.this, ExploreActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.navigation_equipment:

                        break;

                    case R.id.navigation_porter:
                        Intent intent3 = new Intent(EquipmentActivity.this, PorterActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.navigation_home:
                        Intent intent4 = new Intent(EquipmentActivity.this, MainActivity.class);
                        startActivity(intent4);
                        break;

                    case R.id.navigation_preparation:
                        Intent intent5 = new Intent(EquipmentActivity.this, PreparationActivity.class);
                        startActivity(intent5);
                        break;
                }

                return false;
            }
        });
    }
}
