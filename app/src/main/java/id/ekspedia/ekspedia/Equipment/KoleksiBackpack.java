package id.ekspedia.ekspedia.Equipment;

import java.util.ArrayList;

import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class KoleksiBackpack {
    public static ArrayList<Equipment>getEquipment2(){
        ArrayList<Equipment>equipments = new ArrayList<>();
        Equipment equipment2 = null;

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Dry Bag");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Regular Bag");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Carrier");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Tas Kecil");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Eiger");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Consina");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Jack Wolfskin");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        equipment2 = new Equipment();
        equipment2.setDeskripsiEquipment("Tas Eiger");
        equipment2.setImageEquipment(R.drawable.ic_launcher_background);
        equipments.add(equipment2);

        return equipments;
    }
}
