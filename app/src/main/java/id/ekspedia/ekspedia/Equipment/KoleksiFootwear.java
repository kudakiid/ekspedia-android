package id.ekspedia.ekspedia.Equipment;

import java.util.ArrayList;

import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class KoleksiFootwear {

    public static ArrayList<Equipment>getEquipment3(){
        ArrayList<Equipment>equipments2 = new ArrayList<>();
        Equipment equipment3 = null;

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Eiger");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Consina");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Jack Wolfskin");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Converse");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Supreme");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Eiger");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Bathing Ape");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        equipment3 = new Equipment();
        equipment3.setDeskripsiEquipment("Stussy");
        equipment3.setImageEquipment(R.drawable.ic_launcher_background);
        equipments2.add(equipment3);

        return equipments2;
    }
}
