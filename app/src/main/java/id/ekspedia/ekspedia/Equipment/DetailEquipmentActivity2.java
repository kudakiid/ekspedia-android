package id.ekspedia.ekspedia.Equipment;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ekspedia.ekspedia.Explore.*;
import id.ekspedia.ekspedia.Explore.Review;
import id.ekspedia.ekspedia.R;

public class DetailEquipmentActivity2 extends AppCompatActivity {

    ImageView ivEquipment2;
    TextView tvEquipment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_equipment2);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        ivEquipment2 = (ImageView)findViewById(R.id.detail_image_equipment);
        tvEquipment2 = (TextView)findViewById(R.id.price_2);

        Intent i = this.getIntent();
        int gambarEquipment = i.getExtras().getInt("gambar");
        int harga = i.getExtras().getInt("harga");

        ivEquipment2.setImageResource(gambarEquipment);
    }
}
