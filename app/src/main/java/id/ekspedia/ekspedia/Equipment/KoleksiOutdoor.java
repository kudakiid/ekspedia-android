package id.ekspedia.ekspedia.Equipment;

import java.util.ArrayList;

import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class KoleksiOutdoor {

    public static ArrayList<Equipment>getEquipment(){
        ArrayList<Equipment>equipment = new ArrayList<>();
        Equipment equipment1 = null;

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Tenda");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Sleeping Bag");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Trekking Pole");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Lamp");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Dry Bag");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Regular Bag");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Carrier");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        equipment1 = new Equipment();
        equipment1.setDeskripsiEquipment("Tas Eiger");
        equipment1.setImageEquipment(R.drawable.ic_launcher_background);
        equipment.add(equipment1);

        return equipment;
    }

}
