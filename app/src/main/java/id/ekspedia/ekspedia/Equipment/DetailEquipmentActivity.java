package id.ekspedia.ekspedia.Equipment;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import id.ekspedia.ekspedia.R;

public class DetailEquipmentActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DetailEquipmentAdapter adapter;
    private List<DetailEquipment> detailEquipments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_equipment);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_4);
        detailEquipments = new ArrayList<>();
        adapter = new DetailEquipmentAdapter(this, detailEquipments);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10),true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareEquipment();

    }

    private void prepareEquipment() {
        int[] posters = new int[]{
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,
                R.drawable.ic_launcher_background,};

        DetailEquipment a = new DetailEquipment("Tenda", 3, posters[0]);
        detailEquipments.add(a);

        a = new DetailEquipment("Tenda Dome 2 Orang", 5, posters[1]);
        detailEquipments.add(a);

        a = new DetailEquipment("Tenda Biru", 12, posters[2]);
        detailEquipments.add(a);

        a = new DetailEquipment("Lampu", 5, posters[3]);
        detailEquipments.add(a);

        a = new DetailEquipment("Tenda Eiger", 5, posters[4]);
        detailEquipments.add(a);

        a = new DetailEquipment("Jack Wolfskin", 5, posters[5]);
        detailEquipments.add(a);

        a = new DetailEquipment("Consina", 5, posters[6]);
        detailEquipments.add(a);

        a = new DetailEquipment("Panci", 5, posters[7]);
        detailEquipments.add(a);

        a = new DetailEquipment("Panci", 5, posters[8]);
        detailEquipments.add(a);

        a = new DetailEquipment("Panci", 5, posters[9]);
        detailEquipments.add(a);

        adapter.notifyDataSetChanged();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration{
        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f / spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
