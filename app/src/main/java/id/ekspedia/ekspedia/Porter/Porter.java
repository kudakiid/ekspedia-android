package id.ekspedia.ekspedia.Porter;

/**
 * Created by hakimrizki on 13/03/18.
 */

public class Porter {
    int imagePorter;
    String namaPorter;
    String statusPorter;

    public Porter(int imagePorter, String namaPorter, String statusPorter) {
        this.imagePorter = imagePorter;
        this.namaPorter = namaPorter;
        this.statusPorter = statusPorter;
    }

    public int getImagePorter() {
        return imagePorter;
    }

    public void setImagePorter(int imagePorter) {
        this.imagePorter = imagePorter;
    }

    public String getNamaPorter() {
        return namaPorter;
    }

    public void setNamaPorter(String namaPorter) {
        this.namaPorter = namaPorter;
    }

    public String getStatusPorter() {
        return statusPorter;
    }

    public void setStatusPorter(String statusPorter) {
        this.statusPorter = statusPorter;
    }
}
