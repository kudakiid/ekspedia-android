package id.ekspedia.ekspedia.Porter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.ekspedia.ekspedia.ItemClickListener;
import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 13/03/18.
 */

public class PorterAdapter extends RecyclerView.Adapter<PorterAdapter.MyViewHolder> {

    private List<Porter>porters;
    private Context context;
    LayoutInflater inflater;

    public PorterAdapter(Context context, List<Porter>porters) {
        this.porters = porters;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public PorterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list_item_porter, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(PorterAdapter.MyViewHolder holder, int position) {
        final int gambar = porters.get(position).getImagePorter();
        final String nama = porters.get(position).getNamaPorter();
        final String status = porters.get(position).getStatusPorter();

        holder.tv1.setText(nama);
        holder.tv1.setTag(holder);
        holder.tv2.setText(status);
        holder.tv2.setTag(holder);
        holder.imageView.setImageResource(gambar);
        holder.imageView.setTag(holder);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                openDetailActivity(gambar,nama,status);
            }
        });
    }

    @Override
    public int getItemCount() {
        return porters.size();
    }

    private void openDetailActivity(int image, String name, String status) {
        Intent i = new Intent(context, DetailPorterActivity.class);
        i.putExtra("nama", name);
        i.putExtra("gambar", image);
        context.startActivity(i);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tv1,tv2;
        ImageView imageView;
        ItemClickListener itemClickListener;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv1 = (TextView) itemView.findViewById(R.id.list_title);
            tv2 = (TextView) itemView.findViewById(R.id.list_desc);
            imageView = (ImageView)itemView.findViewById(R.id.list_avatar);

            itemView.setOnClickListener(this);

        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }
}
