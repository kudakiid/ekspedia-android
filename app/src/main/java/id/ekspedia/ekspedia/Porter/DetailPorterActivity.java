package id.ekspedia.ekspedia.Porter;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import id.ekspedia.ekspedia.R;

public class DetailPorterActivity extends AppCompatActivity {

    ImageView ivPorter;
    TextView tvPorter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_porter);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        ivPorter = (ImageView)findViewById(R.id.list_avatar_2);
        tvPorter = (TextView)findViewById(R.id.tv_nama_porter);

        Intent i = this.getIntent();
        int gambarPorter = i.getExtras().getInt("gambar");
        String namaPorter = i.getExtras().getString("nama");

        ivPorter.setImageResource(gambarPorter);
        tvPorter.setText(namaPorter);
    }
}
