package id.ekspedia.ekspedia.Porter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import id.ekspedia.ekspedia.BottomNavigationViewHelper;
import id.ekspedia.ekspedia.Equipment.EquipmentActivity;
import id.ekspedia.ekspedia.Explore.ExploreActivity;
import id.ekspedia.ekspedia.MainActivity;
import id.ekspedia.ekspedia.Preparation.PreparationActivity;
import id.ekspedia.ekspedia.R;

public class PorterActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    PorterAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_porter);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        List<Porter>rowListItem = getAllItemList();
        mLayoutManager = new LinearLayoutManager(PorterActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PorterAdapter(PorterActivity.this, rowListItem);
        mRecyclerView.setAdapter(mAdapter);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_explore:
                        Intent intent1 = new Intent(PorterActivity.this, ExploreActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.navigation_equipment:
                        Intent intent2 = new Intent(PorterActivity.this, EquipmentActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.navigation_porter:

                        break;

                    case R.id.navigation_home:
                        Intent intent4 = new Intent(PorterActivity.this, MainActivity.class);
                        startActivity(intent4);
                        break;

                    case R.id.navigation_preparation:
                        Intent intent5 = new Intent(PorterActivity.this, PreparationActivity.class);
                        startActivity(intent5);
                        break;
                }

                return false;
            }
        });
    }

    private List<Porter> getAllItemList() {

        List<Porter> allItems = new ArrayList<>();
        allItems.add(new Porter(R.drawable.home_icon_1,"Zaenal", "Available"));
        allItems.add(new Porter(R.drawable.home_icon_2,"Rohman", "Available"));
        allItems.add(new Porter(R.drawable.home_icon_3,"Jajang", "Available"));
        allItems.add(new Porter(R.drawable.home_icon_5,"Ruslan", "Available"));
        allItems.add(new Porter(R.drawable.home_icon_6,"Ujang", "Available"));
        allItems.add(new Porter(R.drawable.home_icon_1,"Ardi", "Available"));

        return allItems;
    }
}
