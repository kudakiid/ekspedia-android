package id.ekspedia.ekspedia.Preparation;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import id.ekspedia.ekspedia.BottomNavigationViewHelper;
import id.ekspedia.ekspedia.Equipment.EquipmentActivity;
import id.ekspedia.ekspedia.Explore.ExploreActivity;
import id.ekspedia.ekspedia.MainActivity;
import id.ekspedia.ekspedia.Porter.PorterActivity;
import id.ekspedia.ekspedia.R;

public class PreparationActivity extends AppCompatActivity {

    String spinnerLabel ="";
    Spinner sp1;
    EditText edHari, edJumlah;
    RadioGroup rbGroup;
    RadioButton rbButton1, rbButton2;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparation);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        sp1 = (Spinner)findViewById(R.id.sp_preparation);
        edHari = (EditText)findViewById(R.id.ed_estimasi);
        edJumlah = (EditText)findViewById(R.id.ed_jumlah);
        rbGroup = (RadioGroup)findViewById(R.id.rb_decision);
        rbButton1 = (RadioButton)findViewById(R.id.ya);
        rbButton2 = (RadioButton)findViewById(R.id.tidak);
        btnSubmit = (Button)findViewById(R.id.btn_submit);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(4);
        menuItem.setChecked(true);

        sp1.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PreparationActivity.this, EstimationActivity.class);
                startActivity(i);
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_explore:
                        Intent intent1 = new Intent(PreparationActivity.this, ExploreActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.navigation_equipment:
                        Intent intent2 = new Intent(PreparationActivity.this, EquipmentActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.navigation_porter:
                        Intent intent3 = new Intent(PreparationActivity.this, PorterActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.navigation_home:
                        Intent intent4 = new Intent(PreparationActivity.this, MainActivity.class);
                        startActivity(intent4);
                        break;

                    case R.id.navigation_preparation:

                        break;
                }

                return false;
            }
        });
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        String firstItem = String.valueOf(sp1.getSelectedItem());

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            spinnerLabel = adapterView.getItemAtPosition(i).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
