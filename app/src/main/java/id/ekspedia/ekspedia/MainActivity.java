package id.ekspedia.ekspedia;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ekspedia.ekspedia.Equipment.EquipmentActivity;
import id.ekspedia.ekspedia.Explore.ExploreActivity;
import id.ekspedia.ekspedia.Porter.PorterActivity;
import id.ekspedia.ekspedia.Preparation.PreparationActivity;
import id.ekspedia.ekspedia._sliders.FragmentSlider;
import id.ekspedia.ekspedia._sliders.SliderIndicator;
import id.ekspedia.ekspedia._sliders.SliderPagerAdapter;
import id.ekspedia.ekspedia._sliders.SliderView;


public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";

    private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;

    private SliderView sliderView;
    private LinearLayout mLinearLayout;
    private GridView gridView;


    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        sliderView = (SliderView)findViewById(R.id.sliderView);
        mLinearLayout = (LinearLayout)findViewById(R.id.pagesContainer);
        setupSlider();

        gridView = (GridView)findViewById(R.id.grid_view);
        gridView.setAdapter(new HomeAdapter(getApplicationContext()));


        TextView title = (TextView) findViewById(R.id.activityTitle1);
        title.setText("Popular");

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_explore:
                        Intent intent1 = new Intent(MainActivity.this, ExploreActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.navigation_equipment:
                        Intent intent2 = new Intent(MainActivity.this, EquipmentActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.navigation_porter:
                        Intent intent3 = new Intent(MainActivity.this, PorterActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.navigation_home:

                        break;

                    case R.id.navigation_preparation:
                        Intent intent5 = new Intent(MainActivity.this, PreparationActivity.class);
                        startActivity(intent5);
                        break;
                }

                return false;
            }
        });
    }

    private void setupSlider() {
        sliderView.setDurationScroll(800);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(FragmentSlider.newInstance("http://www.menucool.com/slider/prod/image-slider-1.jpg"));
        fragments.add(FragmentSlider.newInstance("http://www.menucool.com/slider/prod/image-slider-2.jpg"));
        fragments.add(FragmentSlider.newInstance("http://www.menucool.com/slider/prod/image-slider-3.jpg"));
        fragments.add(FragmentSlider.newInstance("http://www.menucool.com/slider/prod/image-slider-4.jpg"));

        mAdapter = new SliderPagerAdapter(getSupportFragmentManager(), fragments);
        sliderView.setAdapter(mAdapter);
        mIndicator = new SliderIndicator(this, mLinearLayout, sliderView, R.drawable.indicator_circle);
        mIndicator.setPageCount(fragments.size());
        mIndicator.show();
    }
}
