package id.ekspedia.ekspedia.Explore;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import id.ekspedia.ekspedia.BottomNavigationViewHelper;
import id.ekspedia.ekspedia.Equipment.EquipmentActivity;
import id.ekspedia.ekspedia.MainActivity;
import id.ekspedia.ekspedia.Porter.PorterActivity;
import id.ekspedia.ekspedia.Preparation.PreparationActivity;
import id.ekspedia.ekspedia.R;

public class ExploreActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    public ExploreActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        ExploreAdapter adapter = new ExploreAdapter(this, KoleksiExplore.getExplore());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_explore:

                        break;

                    case R.id.navigation_equipment:
                        Intent intent2 = new Intent(ExploreActivity.this, EquipmentActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.navigation_porter:
                        Intent intent3 = new Intent(ExploreActivity.this, PorterActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.navigation_home:
                        Intent intent4 = new Intent(ExploreActivity.this, MainActivity.class);
                        startActivity(intent4);
                        break;

                    case R.id.navigation_preparation:
                        Intent intent5 = new Intent(ExploreActivity.this, PreparationActivity.class);
                        startActivity(intent5);
                        break;
                }

                return false;
            }
        });
    }
}
