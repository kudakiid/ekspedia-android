package id.ekspedia.ekspedia.Explore;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class Explore {

    int imageExplore;
    String deskripsiExplore;

    public Explore() {
    }

    public int getImageExplore() {
        return imageExplore;
    }

    public void setImageExplore(int imageExplore) {
        this.imageExplore = imageExplore;
    }

    public String getDeskripsiExplore() {
        return deskripsiExplore;
    }

    public void setDeskripsiExplore(String deskripsiExplore) {
        this.deskripsiExplore = deskripsiExplore;
    }
}
