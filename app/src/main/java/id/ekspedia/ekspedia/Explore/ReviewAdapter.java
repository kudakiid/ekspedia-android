package id.ekspedia.ekspedia.Explore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 18/03/18.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    private List<Review> review;
    private Context context;
    private String mYear;


    public ReviewAdapter(Context context, List<Review> review) {
        this.review = review;
        this.context = context;
    }

    @Override
    public ReviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list_item_explore_2, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ReviewAdapter.MyViewHolder holder, int position) {
        final int gambar = review.get(position).getImageReview();
        final String comment = review.get(position).getComment();
        final String nama = review.get(position).getNama();
        final String tanggal = review.get(position).getTanggal();

        holder.tv1.setText(nama);
        holder.tv2.setText(comment);
        holder.imageView.setImageResource(gambar);
        holder.tv3.setText(tanggal);

    }

    @Override
    public int getItemCount() {
        return review.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv1,tv2,tv3;
        ImageView imageView;
        Calendar calendar;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv1 = (TextView) itemView.findViewById(R.id.nama_commenter);
            tv2 = (TextView) itemView.findViewById(R.id.comment_2);
            tv3 = (TextView) itemView.findViewById(R.id.tanggal_comment);
            imageView = (ImageView)itemView.findViewById(R.id.foto_commenter);
            calendar = Calendar.getInstance();
            mYear = String.valueOf(calendar.get(Calendar.YEAR));

        }

        @Override
        public void onClick(View view) {

        }
    }
}
