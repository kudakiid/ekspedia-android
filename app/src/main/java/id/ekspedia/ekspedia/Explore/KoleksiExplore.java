package id.ekspedia.ekspedia.Explore;

import java.util.ArrayList;

import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class KoleksiExplore {

    public static ArrayList<Explore>getExplore(){
        ArrayList<Explore>explores = new ArrayList<>();
        Explore explore = null;

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Rinjani");
        explore.setImageExplore(R.drawable.home_icon_1);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Bromo");
        explore.setImageExplore(R.drawable.home_icon_2);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Raung");
        explore.setImageExplore(R.drawable.home_icon_3);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Merbabu");
        explore.setImageExplore(R.drawable.home_icon_5);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Salak");
        explore.setImageExplore(R.drawable.home_icon_6);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Papandayan");
        explore.setImageExplore(R.drawable.home_icon_1);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Kerinci");
        explore.setImageExplore(R.drawable.home_icon_2);
        explores.add(explore);

        explore = new Explore();
        explore.setDeskripsiExplore("Gunung Semeru");
        explore.setImageExplore(R.drawable.home_icon_3);
        explores.add(explore);

        return explores;
    }
}
