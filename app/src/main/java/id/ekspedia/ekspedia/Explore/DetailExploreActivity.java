package id.ekspedia.ekspedia.Explore;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ekspedia.ekspedia.R;

public class DetailExploreActivity extends AppCompatActivity {

    ImageView ivExplore;
    TextView tvExplore;
    RecyclerView mRecyclerView;
    ReviewAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_explore);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        ivExplore = (ImageView)findViewById(R.id.detail_image_explore);
        tvExplore = (TextView)findViewById(R.id.tv_detail_explore);
        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view_review);
        mRecyclerView.setHasFixedSize(true);
        List<Review>rowListItem = getAllItemList();
        mLayoutManager = new LinearLayoutManager(DetailExploreActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ReviewAdapter(DetailExploreActivity.this, rowListItem);
        mRecyclerView.setAdapter(mAdapter);


        Intent i = this.getIntent();
        int gambarExplore = i.getExtras().getInt("gambar");
        String deskripsi = i.getExtras().getString("deskripsi");

        ivExplore.setImageResource(gambarExplore);
        tvExplore.setText(deskripsi);
    }

    private List<Review>getAllItemList(){
        List<Review>allItem = new ArrayList<>();
        allItem.add(new Review(R.drawable.home_icon_1, "Halo", "Ardi"));
        allItem.add(new Review(R.drawable.home_icon_2, "Bagus nih aplikasinya", "Hakim"));
        allItem.add(new Review(R.drawable.home_icon_3, "Boleh dicoba", "Rafti"));
        allItem.add(new Review(R.drawable.home_icon_5, "Pengalaman baru", "Yuzar"));
        allItem.add(new Review(R.drawable.home_icon_6, "Pengen ke gunung ini", "Yudanto"));
        allItem.add(new Review(R.drawable.home_icon_1, "Kapan ya bisa kesana?", "Ilham"));
        return allItem;
    }
}
