package id.ekspedia.ekspedia.Explore;

import java.util.Calendar;

/**
 * Created by hakimrizki on 18/03/18.
 */

public class Review {
    int imageReview;
    String comment;
    String nama;
    String tanggal;

    public Review(int imageReview, String comment, String nama) {
        this.imageReview = imageReview;
        this.comment = comment;
        this.nama = nama;
    }

    public int getImageReview() {
        return imageReview;
    }

    public void setImageReview(int imageReview) {
        this.imageReview = imageReview;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
