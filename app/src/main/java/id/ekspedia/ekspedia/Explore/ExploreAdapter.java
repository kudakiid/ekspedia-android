package id.ekspedia.ekspedia.Explore;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.ekspedia.ekspedia.ItemClickListener;
import id.ekspedia.ekspedia.R;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.RecyclerViewHolderExplore>{

    Context context;
    LayoutInflater inflater;
    ArrayList<Explore> explores;


    public ExploreAdapter(Context context, ArrayList<Explore> explores) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.explores = explores;
    }

    @Override
    public RecyclerViewHolderExplore onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list_item_explore,parent,false);
        RecyclerViewHolderExplore viewHolderExplore = new RecyclerViewHolderExplore(v);
        return viewHolderExplore;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolderExplore holder, int position) {
        final int gambar = explores.get(position).getImageExplore();
        final String deskripsi = explores.get(position).getDeskripsiExplore();

        holder.imageExplore.setImageResource(gambar);
        holder.tvExplore.setText(deskripsi);
        holder.imageExplore.setTag(holder);
        holder.tvExplore.setTag(holder);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                openDetailActivity(deskripsi, gambar);
            }
        });

    }

    @Override
    public int getItemCount() {
        return explores.size();
    }

    private void openDetailActivity(String deskripsi, int image){
        Intent i = new Intent(context, DetailExploreActivity.class);
        i.putExtra("deskripsi",deskripsi);
        i.putExtra("gambar",image);
        context.startActivity(i);
    }

    public class RecyclerViewHolderExplore extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvExplore;
        ImageView imageExplore;
        ItemClickListener itemClickListener;


        public RecyclerViewHolderExplore(View itemView) {
            super(itemView);

            tvExplore = (TextView) itemView.findViewById(R.id.deskripsi_explore);
            imageExplore = (ImageView) itemView.findViewById(R.id.image_explore);

            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }
}
