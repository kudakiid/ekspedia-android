package id.ekspedia.ekspedia;

/**
 * Created by hakimrizki on 11/03/18.
 */

public interface ItemClickListener {
    void onItemClick(int pos);
}
